package DataLayer;

import BusinessLayer.MenuItem;


import java.io.*;
import java.util.HashSet;


public class RestaurantSerializator{
    private static final String FILE="C:\\Users\\Deni\\Desktop\\PT\\PT2019\\pt2019_30222_idu_irina-denisa_assignment_4\\Menu.ser";

    public void save(HashSet<MenuItem> menuItem) {

        try {
            FileOutputStream fileOutputStream=new FileOutputStream(FILE);
            ObjectOutputStream outputStream=new ObjectOutputStream(fileOutputStream);
            outputStream.writeObject(menuItem);
            outputStream.close();
            fileOutputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public HashSet<MenuItem> load(){
        HashSet<MenuItem> menuItem=new HashSet<>();
        try {
            FileInputStream fileInputStream=new FileInputStream(FILE);
            ObjectInputStream in=new ObjectInputStream(fileInputStream);
            menuItem=(HashSet<MenuItem>)in.readObject();
            in.close();
            fileInputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return menuItem;
    }




}
