package PresentationLayer;

import MVC.MVC;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddOrderView {
    JFrame frame;
    JComboBox comboBox;
    JButton button,button1;
    JLabel l1,l2;
    JTextField tf1;

    public AddOrderView(MVC mvc){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(500,300);
        frame.setLocationRelativeTo(null);

        l1=new JLabel("Add produs in comada");
        button=new JButton("Add produs");
        button1=new JButton("Finalizare");
        l2=new JLabel("Masa: ");
        tf1=new JTextField();

        mvc.ShowProduse(AddOrderView.this);


        l1.setBounds(50,50,250,30);
        l2.setBounds(250,50,50,30);
        tf1.setBounds(300,50,100,30);
        button.setBounds(50,180,150,30);
        button1.setBounds(230,180,150,30);


        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mvc.addProdus(comboBox.getSelectedItem().toString());
            }
        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mvc.plasareC(Integer.parseInt(tf1.getText()));
                frame.setVisible(false);
            }
        });

        frame.add(l1);
        frame.add(button);
        frame.add(button1);
        frame.add(l2);
        frame.add(tf1);



    }

    public void populare(String[] data){
        comboBox =new JComboBox(data);
        comboBox.setBounds(170,100,100,30);
        frame.add(comboBox);


    }
}
