package PresentationLayer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminView {
    JFrame frame;
    JButton btn1,btn2,btn3,btn4;

    public AdminView(){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(375,400);
        frame.setLocationRelativeTo(null);

        btn1=new JButton("Add MenuItem");
        btn2=new JButton("Edit MenuItem");
        btn3=new JButton("Delete MenuItem");
        btn4=new JButton("View MenuItem");

        btn1.setBounds(50,50,200,30);
        btn2.setBounds(50,120,200,30);
        btn3.setBounds(50,190,200,30);
        btn4.setBounds(50,260,200,30);

        frame.add(btn1);
        frame.add(btn2);
        frame.add(btn3);
        frame.add(btn4);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AddMenuItemView addMenuItemView=new AddMenuItemView();
            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EditMenuItemView editMenuItemView=new EditMenuItemView();
            }
        });
        btn3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DeleteMenuItemView deleteMenuItemView=new DeleteMenuItemView();
            }
        });

        btn4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ViewMenuItemView viewMenuItemView=new ViewMenuItemView();
            }
        });




    }

}
