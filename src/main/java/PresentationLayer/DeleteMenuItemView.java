package PresentationLayer;

import MVC.MVC;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeleteMenuItemView {
    JFrame frame;
    JComboBox comboBox;
    JLabel label;
    JButton jButton;
    public DeleteMenuItemView(){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(500,500);
        frame.setLocationRelativeTo(null);

        label=new JLabel("Alege produsul pe care il stergeti");
        jButton=new JButton("Delete");

        label.setBounds(70,70,200,30);
        jButton.setBounds(100,170,100,30);


        MVC mvc=new MVC();
        mvc.ShowCombo(DeleteMenuItemView.this);


        frame.add(label);
        frame.add(jButton);

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String comp=comboBox.getSelectedItem().toString();
                mvc.deleteProduct(comp);

                frame.setVisible(false);

            }
        });



    }

    public void populare(String[] data){
        comboBox =new JComboBox(data);
        comboBox.setBounds(150,120,100,30);
        frame.add(comboBox);


    }
}
