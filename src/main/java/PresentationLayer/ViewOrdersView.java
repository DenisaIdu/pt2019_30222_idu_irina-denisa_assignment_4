package PresentationLayer;

import MVC.MVC;

import javax.swing.*;
import java.util.ArrayList;

public class ViewOrdersView {
    JFrame frame;
    JTable jTable;
    public ViewOrdersView(MVC mvc){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setSize(500,500);
        frame.setLocationRelativeTo(null);

        mvc.ViewOrders(ViewOrdersView.this);

    }

    public void populare(String[][] data){
        String[] column={"Nr","Produs","Pret"};
        jTable=new JTable(data,column);
        frame.add(jTable);
    }

    public JTable getjTable() {
        return jTable;
    }
}
