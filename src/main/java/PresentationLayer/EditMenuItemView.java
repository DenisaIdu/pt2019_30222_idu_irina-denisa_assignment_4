package PresentationLayer;

import MVC.MVC;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EditMenuItemView {
    JFrame frame;
    JComboBox comboBox,comboBox1,comboBox2;
    JLabel l1,l2,l3;
    JButton btn1,btn2,btn3;

    public EditMenuItemView(){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(500,500);
        frame.setLocationRelativeTo(null);

        l1=new JLabel("Produsul pe care doriti sa il modificati: ");
        l2=new JLabel("Componenta pe care doriti sa o stergeti: ");
        l3=new JLabel("Add Componenta noua");
        btn1=new JButton("Add");
        btn2=new JButton("Delete");
        btn3=new JButton("Cauta");

        l1.setBounds(50,50,300,30);
        l2.setBounds(50,150,300,30);
        l3.setBounds(50,250,300,30);
        btn1.setBounds(50,350,100,30);
        btn2.setBounds(180,350,100,30);
        btn3.setBounds(300,100,100,30);

        comboBox1=new JComboBox();
        comboBox1.setBounds(70,200,200,30);
        frame.add(comboBox1);

        MVC mvc=new MVC();
        mvc.ShowCombo(EditMenuItemView.this);
        mvc.ShowComboBase(EditMenuItemView.this);

        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                comboBox1.removeAllItems();
                mvc.ShowComboComponente(EditMenuItemView.this,comboBox.getSelectedItem().toString());
            }
        });
        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mvc.editCP(comboBox.getSelectedItem().toString(),comboBox2.getSelectedItem().toString(),1);
                frame.setVisible(false);
            }
        });

        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mvc.editCP(comboBox.getSelectedItem().toString(),comboBox1.getSelectedItem().toString(),2);
                frame.setVisible(false);
            }
        });

        frame.add(l1);
        frame.add(l2);
        frame.add(l3);
        frame.add(btn1);
        frame.add(btn2);
        frame.add(btn3);
    }

    public void populare(String[] data,int combo){
        if (combo==0){
            comboBox =new JComboBox(data);
            comboBox.setBounds(70,100,200,30);
            frame.add(comboBox);

        }
        if (combo==1){
            for (int i=0;i<data.length;i++){
                comboBox1.addItem(data[i]);
            }

        }
        if (combo==2){
            comboBox2=new JComboBox(data);
            comboBox2.setBounds(70,300,200,30);
            frame.add(comboBox2);
        }

    }

    public JComboBox getComboBox() {
        return comboBox;
    }
}
