package PresentationLayer;

import BusinessLayer.Restaurant;
import MVC.MVC;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddCompositeView {
    JFrame frame;
    JTextField textField;
    JButton btn,btn2;
    JLabel l1;
    JComboBox comboBox;

    public AddCompositeView () {
        frame = new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);

        textField = new JTextField();
        l1 = new JLabel("Nume");
        btn = new JButton("Add component");
        btn2=new JButton("Add product");



        MVC mvc=new MVC();
        mvc.ShowCombo(AddCompositeView.this);

        l1.setBounds(50, 50, 100, 30);
        textField.setBounds(150, 50, 100, 30);

        btn.setBounds(30,170,150,30);
        btn2.setBounds(200,170,150,30);


        frame.add(l1);
        frame.add(textField);
        frame.add(btn);
        frame.add(btn);
        frame.add(btn2);

        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                String name=textField.getText();
                String comp=comboBox.getSelectedItem().toString();

                mvc.splitCombo(name,comp);

            }
        });

        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mvc.addCP();
                frame.setVisible(false);
            }
        });

    }

    public void populare(String[] data){
        comboBox =new JComboBox(data);
        comboBox.setBounds(150,120,100,30);
        frame.add(comboBox);


    }

}
