package PresentationLayer;

import MVC.MVC;

import javax.swing.*;

public class ViewMenuItemView {
    JFrame frame;
    JTable jTable;
    public ViewMenuItemView(){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setSize(500,500);
        frame.setLocationRelativeTo(null);
        MVC mvc=new MVC();
        mvc.ShowMenuList(ViewMenuItemView.this);
    }

    public void populare(String[][] data){
        String[] columns={"Nume Produs", "Pret"};
        jTable=new JTable(data,columns);
        frame.add(jTable);


    }

}
