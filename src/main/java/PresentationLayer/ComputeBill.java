package PresentationLayer;

import MVC.MVC;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ComputeBill {
    JFrame frame;
    JLabel l1;
    JTextField tf1;
    JButton btn;

    public ComputeBill(MVC mvc){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(400,300);
        frame.setLocationRelativeTo(null);

        l1=new JLabel("Selecteaza comanda ptr factura");
        tf1=new JTextField();
        btn=new JButton("Compute bill");

        l1.setBounds(50,50,300,30);
        tf1.setBounds(100,100,100,30);
        btn.setBounds(200,170,150,30);

        frame.add(l1);
        frame.add(tf1);
        frame.add(btn);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mvc.computeBill(tf1.getText());
            }
        });

    }
}
