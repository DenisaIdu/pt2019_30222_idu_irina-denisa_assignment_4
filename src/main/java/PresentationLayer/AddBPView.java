package PresentationLayer;

import BusinessLayer.Restaurant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddBPView {
    JFrame frame;
    JTextField textField, textField2;
    JButton btn;
    JLabel l1, l2;
    JComboBox comboBox;

    public AddBPView() {
        frame = new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setLocationRelativeTo(null);

        textField = new JTextField();
        textField2 = new JTextField();
        l1 = new JLabel("Nume");
        l2 = new JLabel("Pret");
        btn = new JButton("Add");

        l1.setBounds(50, 50, 100, 30);
        textField.setBounds(150, 50, 100, 30);
        l2.setBounds(50, 100, 100, 30);
        textField2.setBounds(150, 100, 100, 30);
        btn.setBounds(100, 200, 100, 30);

        frame.add(l1);
        frame.add(l2);
        frame.add(textField);
        frame.add(textField2);
        frame.add(btn);

        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = textField.getText();
                float pret = Float.parseFloat(textField2.getText());

                Restaurant restaurant = new Restaurant();
                restaurant.createNewMenuItemBP(name, pret);

                frame.setVisible(false);


            }
        });

    }
}
