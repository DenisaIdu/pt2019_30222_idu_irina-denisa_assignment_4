package PresentationLayer;

import BusinessLayer.Restaurant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    public static void main(String[] args){
        JFrame frame;
        JButton btn1, btn2,btn3;
        frame=new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(300,300);
        frame.setLocationRelativeTo(null);

        btn1=new JButton("Admin");
        btn2=new JButton("Chef");
        btn3=new JButton("Waiter");

        btn1.setBounds(50,50,150,30);
        btn2.setBounds(50,120,150,30);
        btn3.setBounds(50,190,150,30);

        frame.add(btn1);
        frame.add(btn2);
        frame.add(btn3);

        Restaurant restaurant=new Restaurant();

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AdminView adminView=new AdminView();
            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ChefView chefView=new ChefView();
            }
        });

        btn3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                WaiterView waiterView=new WaiterView();
            }
        });


    }
}
