package PresentationLayer;

import MVC.MVC;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaiterView {
    JFrame frame;
    JButton btn1,btn2,btn3;

    public WaiterView(){
        frame=new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(375,300);
        frame.setLocationRelativeTo(null);

        btn1=new JButton("Add Order");
        btn2=new JButton("View Orders");
        btn3=new JButton("Compute bill");


        btn1.setBounds(50,50,200,30);
        btn2.setBounds(50,120,200,30);
        btn3.setBounds(50,190,200,30);

        MVC mvc=new MVC();

        frame.add(btn1);
        frame.add(btn2);
        frame.add(btn3);

        btn1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AddOrderView addOrderView=new AddOrderView(mvc);
            }
        });

        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ViewOrdersView viewOrdersView=new ViewOrdersView(mvc);
            }
        });
        btn3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComputeBill computeBill=new ComputeBill(mvc);
            }
        });






    }
}
