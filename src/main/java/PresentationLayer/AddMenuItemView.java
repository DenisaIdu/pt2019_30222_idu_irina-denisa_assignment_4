package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.Restaurant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddMenuItemView {
    JFrame frame;
    JButton btn1,btn2;


    public AddMenuItemView() {
        frame = new JFrame();
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setSize(400, 200);
        frame.setLocationRelativeTo(null);

        btn1 = new JButton("Add Base Product");
        btn2 = new JButton("Add Composit Product");

        btn1.setBounds(50, 50, 200, 30);
        btn2.setBounds(50, 100, 200, 30);

        frame.add(btn1);
        frame.add(btn2);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddBPView adminView = new AddBPView();
            }
        });

        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddCompositeView compositeView=new AddCompositeView();
            }
        });


    }






}
