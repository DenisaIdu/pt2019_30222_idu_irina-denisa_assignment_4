package MVC;

import BusinessLayer.*;
import PresentationLayer.*;

import javax.xml.crypto.Data;
import java.lang.reflect.Type;
import java.util.*;

public class MVC {
    Restaurant restaurant;
    MenuItem menuItem;
    CompositeProduct compositeProduct;
    String nameCp=null;
    ArrayList<MenuItem> comanda;
    int idComanda=0;

    public MVC(){
        restaurant=new Restaurant();
        compositeProduct=new CompositeProduct();

    }

    public void ShowMenuList(ViewMenuItemView menuItemView){
        restaurant=new Restaurant();
        String[][] data=new String[restaurant.getMenuItemsHS().size()][2];
        Iterator<MenuItem> iterator=restaurant.getMenuItemsHS().iterator();
        int i=0;
        while (iterator.hasNext()){
            MenuItem menuItem=iterator.next();
            data[i][0]=menuItem.getName();
            data[i][1]= String.valueOf(menuItem.getPret());
            i++;
        }
        menuItemView.populare(data);
    }

    public void ShowCombo(AddCompositeView addCompositeView){
        restaurant=new Restaurant();
        String[] data=new String[restaurant.getMenuItemsHS().size()];
        Iterator<MenuItem> iterator=restaurant.getMenuItemsHS().iterator();
        int i=0;
        while (iterator.hasNext()){
            MenuItem menuItem=iterator.next();
            data[i]=menuItem.getName();
            data[i]+="-"+menuItem.getPret();
            i++;
        }
        addCompositeView.populare(data);
    }



    public void ShowComboComponente(EditMenuItemView editMenuItemView, String comp) {
        restaurant=new Restaurant();
        String compName = null;
        String compPret = null;
        int i = 0;
        for (String val : comp.split("-", 2)) {
            if (i == 0) {
                compName = val;
                i++;
            }
            if (i == 1) {
                compPret = val;
            }
        }
        float compPPret = Float.parseFloat(compPret);
        MenuItem menuItem = new CompositeProduct(compName, compPPret);
        String[]data=null;

        Iterator<MenuItem> iterator = restaurant.getMenuItemsHS().iterator();
        while (iterator.hasNext()) {
            MenuItem menuItem1 = iterator.next();
            if (menuItem1 instanceof CompositeProduct) {
                if (menuItem1.getName().equals(menuItem.getName())) {
                    data = new String[((CompositeProduct) menuItem1).getMenuItems().size()];
                    for (i = 0; i < ((CompositeProduct) menuItem1).getMenuItems().size(); i++) {
                        data[i] = ((CompositeProduct) menuItem1).getMenuItems().get(i).getName();
                        data[i] += "-" + ((CompositeProduct) menuItem1).getMenuItems().get(i).getPret();

                    }
                }

            }
        }
    editMenuItemView.populare(data,1);
    }

    public void editCP(String nameCp,String nameBP,int op){
        restaurant=new Restaurant();
        String compCPName = null;
        String compCPPret = null;
        int i = 0;
        for (String val : nameCp.split("-", 2)) {
            if (i == 0) {
                compCPName = val;
                i++;
            }
            if (i == 1) {
                compCPPret = val;
            }
        }
        float compPPret = Float.parseFloat(compCPPret);
        MenuItem menuItemCP = new CompositeProduct(compCPName, compPPret);

        String compBPName = null;
        String compBPPret = null;
        i = 0;
        for (String val : nameBP.split("-", 2)) {
            if (i == 0) {
                compBPName = val;
                i++;
            }
            if (i == 1) {
                compBPPret = val;
            }
        }
        compPPret = Float.parseFloat(compBPPret);
        MenuItem menuItemBP = new BaseProduct(compBPName, compPPret);
        restaurant.editMenuItem(menuItemCP,menuItemBP,op);
    }

    public void ShowCombo(EditMenuItemView addCompositeView){
        restaurant=new Restaurant();
        String[] data=new String[restaurant.getMenuItemsHS().size()];
        Iterator<MenuItem> iterator=restaurant.getMenuItemsHS().iterator();
        int i=0;
        while (iterator.hasNext()){
            MenuItem menuItem=iterator.next();
            if (menuItem instanceof CompositeProduct){
                data[i]=menuItem.getName();
                data[i]+="-"+menuItem.getPret();
                i++;
            }
        }
        addCompositeView.populare(data,0);
    }

    public void ShowComboBase(EditMenuItemView addCompositeView){
        restaurant=new Restaurant();
        String[] data=new String[restaurant.getMenuItemsHS().size()];
        Iterator<MenuItem> iterator=restaurant.getMenuItemsHS().iterator();
        int i=0;
        while (iterator.hasNext()){
            MenuItem menuItem=iterator.next();
            if (menuItem instanceof BaseProduct){
                data[i]=menuItem.getName();
                data[i]+="-"+menuItem.getPret();
                i++;
            }
        }
        addCompositeView.populare(data,2);
    }

    public void ShowCombo(DeleteMenuItemView addCompositeView){
        restaurant=new Restaurant();
        String[] data=new String[restaurant.getMenuItemsHS().size()];
        Iterator<MenuItem> iterator=restaurant.getMenuItemsHS().iterator();
        int i=0;
        while (iterator.hasNext()){
            MenuItem menuItem=iterator.next();
            data[i]=menuItem.getName();
            data[i]+="-"+menuItem.getPret();
            i++;
        }
        addCompositeView.populare(data);
    }

   public void addProdus(String comp){
       String compName = null;
       String compPret = null;
       int i = 0;
       for (String val : comp.split("-", 2)) {
           if (i == 0) {
               compName = val;
               i++;
           }
           if (i == 1) {
               compPret = val;
           }
       }
       float compPPret = Float.parseFloat(compPret);
       MenuItem menuItem = new BaseProduct(compName, compPPret);

       Iterator<MenuItem> iterator=restaurant.getMenuItemsHS().iterator();
       while (iterator.hasNext()){
           MenuItem menuItem1=iterator.next();
           if (menuItem1.getName().equals(menuItem.getName())){
               comanda.add(menuItem1);
           }

       }
   }

   public void plasareC(int masa){
        idComanda++;
       Date date=new Date();
       Order order=new Order(idComanda,date,masa);

       restaurant.createNewOrder(order,comanda);
   }

    public void ShowProduse(AddOrderView addCompositeView){
        comanda=new ArrayList<>();
        String[] data=new String[restaurant.getMenuItemsHS().size()];
        Iterator<MenuItem> iterator=restaurant.getMenuItemsHS().iterator();
        int i=0;
        while (iterator.hasNext()){
            MenuItem menuItem=iterator.next();
            data[i]=menuItem.getName();
            data[i]+="-"+menuItem.getPret();
            i++;
        }
        addCompositeView.populare(data);
    }

    public void splitCombo(String name, String comp){
        String compName=null;
        String compPret=null;
        int i=0;
        for (String val: comp.split("-",2)){
            if (i==0){
                compName=val;
                i++;
            }
            if (i==1){
                compPret= val;
            }
        }
        float compPPret= Float.parseFloat(compPret);
        MenuItem menuItem=new BaseProduct(compName,compPPret);
        compositeProduct.addMenuItem(menuItem);
        compositeProduct.setName(name);
        nameCp=name;

    }

    public void deleteProduct(String comp){
        restaurant=new Restaurant();

        String compName=null;
        String compPret=null;
        int i=0;
        for (String val: comp.split("-",2)){
            if (i==0){
                compName=val;
                i++;
            }
            if (i==1){
                compPret= val;
            }
        }
        float compPPret= Float.parseFloat(compPret);
        menuItem=new CompositeProduct(compName,compPPret);
        restaurant.deleteMenuItem(menuItem);
    }

    public void addCP(){
        restaurant=new Restaurant();
        restaurant.createNewMenuItemCP(nameCp,compositeProduct);
    }

    public void ViewOrders(ViewOrdersView viewOrdersView){
        System.out.println("AAAAAAAA");
        String[][] data=new String[1000][3];
        int i=0;

        for(Map.Entry<Order,ArrayList<MenuItem>> listEntry: restaurant.getMap().entrySet()){
            float pret=0;
            for (MenuItem menuItem: listEntry.getValue()){
                data[i][0]= String.valueOf(listEntry.getKey().getOrderID());
                data[i][1]= menuItem.getName();
                data[i][2]= String.valueOf(menuItem.getPret());
                pret=pret+menuItem.getPret();
                i++;
            }
            data[i][0]= String.valueOf(listEntry.getKey().getOrderID());
            data[i][1]=listEntry.getKey().getDate().toString();
            data[i][2]= String.valueOf(restaurant.computePrice(listEntry.getKey().getOrderID()));
            i=i+2;
        }
        viewOrdersView.populare(data);
    }

    public void computeBill(String orderNr){
        restaurant.generateBill(Integer.parseInt(orderNr));
    }
}
