package BusinessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
    static final long serialVersionUID = -7588980448693010399L;

    private String name;
    private float pret;
     public MenuItem(){

     }

    public void setPret(float pret) {
        this.pret = pret;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPret() {
        return pret;
    }

    public String getName() {
        return name;
    }

    public abstract float computePrice();

}
