package BusinessLayer;

public class BaseProduct extends MenuItem {
    static final long serialVersionUID = -7588980448693010399L;

    public BaseProduct(){

    }

    public BaseProduct(String name, float pret){
        super.setPret(pret);
        super.setName(name);
    }


    @Override
    public float computePrice() {
        return this.getPret();
    }
}
