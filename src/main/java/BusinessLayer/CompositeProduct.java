package BusinessLayer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
    static final long serialVersionUID = -7588980448693010399L;

    private ArrayList<MenuItem> menuItems;

    public CompositeProduct(){
        menuItems=new ArrayList<>();
    }


    public CompositeProduct(String name, float pret){
        super.setName(name);
        super.setPret(pret);
        menuItems=new ArrayList<>();
    }



    public float computePrice(){
        float pret=0;
        for (MenuItem menuItem:menuItems)
        {
            pret+=menuItem.computePrice();
        }

        super.setPret(pret);
        return pret;
    }

    public void setMenuItems(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public void addMenuItem(MenuItem menuItem){
        menuItems.add(menuItem);
    }

    public void removeMenuItem(MenuItem menuItem){
        menuItems.remove(menuItem);

    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }
}
