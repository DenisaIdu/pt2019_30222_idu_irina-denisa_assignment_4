package BusinessLayer;

import DataLayer.RestaurantSerializator;
import PresentationLayer.ChefView;

import java.io.File;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.*;

public class Restaurant extends Observable  implements RestaurantProcessing, Serializable{

    HashSet<MenuItem> menuItemsHS;
    public HashMap<Order,ArrayList<MenuItem>> map=new HashMap<>();
    public ChefView chef;
    private RestaurantSerializator restaurantSerializator;

    public Restaurant(ChefView chef){
        addObserver(chef);
    }


    public Restaurant(){
        menuItemsHS=new HashSet<>();
        restaurantSerializator=new RestaurantSerializator();
        menuItemsHS=restaurantSerializator.load();
        //afisare();
    }

    public void createNewMenuItemBP(String name,float pret) {
        assert name!=null : "Name is null";
        assert pret!=0: "Price is 0";

        int preNrOfMenuItem=menuItemsHS.size();
        int postNrOfMenuItme=0;
        MenuItem menuItem=new BaseProduct(name,pret);
        menuItemsHS.add(menuItem);

        postNrOfMenuItme=menuItemsHS.size();

        assert preNrOfMenuItem+1==postNrOfMenuItme : "Add Base Product failed!";



        restaurantSerializator.save(menuItemsHS);

    }

    public void createNewMenuItemCP(String name,CompositeProduct compositeProduct) {
        assert name!=null : "Name is null";
        assert compositeProduct!=null: "Composit Product is null";

        int preNrOfMenuItem=menuItemsHS.size();
        int postNrOfMenuItme=0;

        compositeProduct.computePrice();
        MenuItem menuItem=new CompositeProduct();
        menuItem=compositeProduct;

        menuItemsHS.add(menuItem);

        postNrOfMenuItme=menuItemsHS.size();
        assert preNrOfMenuItem+1==postNrOfMenuItme : "Add Composite Product failed!";



        restaurantSerializator.save(menuItemsHS);
    }

    public void deleteMenuItem(MenuItem menuItem) {
        assert menuItem!=null : "MenuItem is null";

        int preNrOfMenuItem=menuItemsHS.size();
        int postNrOfMenuItme=0;
        MenuItem menuItemSters=null;
        try {
            Iterator<MenuItem> iterator = menuItemsHS.iterator();
            while (iterator.hasNext()) {
                MenuItem menuItem2 = iterator.next();
                if (menuItem.getName().equals(menuItem2.getName())) {
                    menuItemSters=menuItem2;
                    //System.out.println("Sters: "+menuItem2+menuItemSters);
                    menuItemsHS.remove(menuItem2);

                }
            }

        }catch (ConcurrentModificationException e){
            Iterator<MenuItem> iterator = menuItemsHS.iterator();
            while (iterator.hasNext()){
                MenuItem menuItem1=iterator.next();
               // System.out.println(menuItem1);
               if (menuItem1 instanceof CompositeProduct) {
                   //System.out.println("CP: "+menuItem1);
                  for (int i=0;i<((CompositeProduct) menuItem1).getMenuItems().size();i++){
                      if (((CompositeProduct) menuItem1).getMenuItems().get(i).getName().equals(menuItemSters.getName())){
                          //System.out.println("Continr"+menuItemSters.getName());
                          ((CompositeProduct) menuItem1).removeMenuItem(((CompositeProduct) menuItem1).getMenuItems().get(i));
                          menuItem1.computePrice();
                      }

                  }
               }

            }
        }

        postNrOfMenuItme=menuItemsHS.size();

        assert preNrOfMenuItem+1==postNrOfMenuItme : "Delete item failed!";
        restaurantSerializator.save(menuItemsHS);

    }

    public void editMenuItem(MenuItem menuItemCP, MenuItem menuItemBP, int tipOp) {

        Iterator<MenuItem> iterator = menuItemsHS.iterator();
        while (iterator.hasNext()) {
            MenuItem menuItem1 = iterator.next();
            if (menuItem1 instanceof CompositeProduct) {
                if (menuItem1.getName().equals(menuItemCP.getName())) {
                    Iterator<MenuItem> iterator2 = menuItemsHS.iterator();
                    while (iterator2.hasNext()) {
                        MenuItem menuItem2 = iterator2.next();
                        if (menuItem2 instanceof BaseProduct) {
                            if (menuItem2.getName().equals(menuItemBP.getName())) {
                                if (tipOp==1)
                                    ((CompositeProduct) menuItem1).addMenuItem(menuItem2);
                                else
                                    if (tipOp==2)
                                        ((CompositeProduct) menuItem1).removeMenuItem(menuItem2);
                                menuItem1.computePrice();
                                restaurantSerializator.save(menuItemsHS);
                            }
                        }
                    }
                }

            }
        }


    }

    public void createNewOrder(Order order, ArrayList<MenuItem> menuItems) {
       assert order!=null: "Order is null";
       assert  menuItems!=null: "MenuItems is null";
        map.put(order,menuItems);
    }

    public float computePrice(int idCompanda) {
        float pret=0;
        for(Map.Entry<Order,ArrayList<MenuItem>> listEntry: map.entrySet()){
            if (listEntry.getKey().getOrderID()==idCompanda)
                for (MenuItem menuItem: listEntry.getValue()){
                    pret=pret+menuItem.getPret();
                }
        }

        return pret;
    }

    public void generateBill(int orderNr) {
        File file=new File("bills"+orderNr+".txt");
        try {
            PrintWriter writer=new PrintWriter(file,"UTF-8");
            for(Map.Entry<Order,ArrayList<MenuItem>> listEntry: map.entrySet()){
                if (listEntry.getKey().getOrderID()==orderNr){
                    writer.println("Order ID: "+listEntry.getKey().getOrderID());
                    writer.println("Masa: "+listEntry.getKey().getTable());
                    writer.println("Data: "+listEntry.getKey().getDate());

                    for (MenuItem menuItem: listEntry.getValue()) {
                        writer.println("Nume produs: "+menuItem.getName()+"     Pret: "+menuItem.getPret());
                    }
                    writer.println("Total comanda:   "+computePrice(listEntry.getKey().getOrderID()));
                    writer.close();
                }



            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    public void afisare(){
        Iterator<MenuItem> iterator=menuItemsHS.iterator();
        while (iterator.hasNext()){
            MenuItem menuItem=iterator.next();
            System.out.println("MenuItem: "+menuItem.getName());
            if (menuItem instanceof CompositeProduct){
                for (int i=0;i<((CompositeProduct) menuItem).getMenuItems().size();i++){
                    System.out.println(((CompositeProduct) menuItem).getMenuItems().get(i).getName());
                }
            }
        }
    }

    public RestaurantSerializator getRestaurantSerializator() {
        return restaurantSerializator;
    }

    public HashSet<MenuItem> getMenuItemsHS() {
        return menuItemsHS;
    }

    public HashMap<Order, ArrayList<MenuItem>> getMap() {
        return map;
    }
}
