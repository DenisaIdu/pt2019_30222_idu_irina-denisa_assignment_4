package BusinessLayer;

import java.util.Date;

public class Order {
    private int orderID;
    private Date date;
    private int table;

    public Order(int id,Date date,int table){
        this.orderID=id;
        this.date=date;
        this.table=table;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public Date getDate() {
        return date;
    }

    public int getOrderID() {
        return orderID;
    }

    public int getTable() {
        return table;
    }
}
