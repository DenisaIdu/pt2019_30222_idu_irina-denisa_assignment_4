package BusinessLayer;

import java.util.ArrayList;

public interface RestaurantProcessing {

    /**
     *
     * @pre name!=null pret!=null
     * @post postAccSize=preAccSize+1
     * @param name
     * @param pret
     */
    void createNewMenuItemBP(String name,float pret);

    /**
     * @pre nume!=null composite!=null
     * @post postAccSize=preAccSize+1
     * @param name
     * @param compositeProduct
     */
    void createNewMenuItemCP(String name,CompositeProduct compositeProduct);
    void deleteMenuItem(MenuItem menuItem);
    void editMenuItem(MenuItem menuItemCP, MenuItem BP, int tipOp);

    void createNewOrder(Order order, ArrayList<MenuItem> menuItems);
    float computePrice(int idComanda);
    void generateBill(int orderNr);
    void afisare();

}
